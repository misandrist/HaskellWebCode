{-# LANGUAGE OverloadedStrings #-}

module Introduction.SimpleSite where

import Happstack.Server (Conf(..), nullConf, simpleHTTP, ok)
import Data.Text (Text)

run :: IO ()
run = simpleHTTP config $ ok updog

config :: Conf
config = nullConf {port=8000}

updog :: Text
updog = "What's updog!"
