{-# LANGUAGE QuasiQuotes  #-}

module Introduction.SimpleRoutes where

import Control.Monad (msum)
import Happstack.Server (Conf(..), nullConf, simpleHTTP, dir, ServerPartT)
import HSP
import Happstack.Server.HSP.HTML()
import HSP.Monad                  (HSPT(..))
import Language.Haskell.HSX.QQ    (hsx)

run :: IO ()
run = simpleHTTP config $ msum
      [
        dir "dog" $ updog,
        dir "cat" $ hikitty,
        index
       ]

config :: Conf
config = nullConf {port=8001}

index :: ServerPartT IO XML
index = unHSPT $ unXMLGenT
        [hsx|
  <html>
   <head>
   <title>CATS OR DOGS!!!</title>
   </head>
   <body>
    <h1>Cats or Dogs!</h1>
    <ul>
        <li><a href="/cat">Cat!</a></li>
        <li><a href="/dog">Dog!</a></li>
    </ul>
    </body>
  </html> :: XMLGenT (HSPT XML (ServerPartT IO)) XML
|]

updog :: ServerPartT IO XML
updog = unHSPT $ unXMLGenT
        [hsx|
<html>
<head><title>DOG!!!</title></head>
<body><h1>"What's updog!"</h1>
</body>
</html> :: XMLGenT (HSPT XML (ServerPartT IO)) XML
|]

hikitty :: ServerPartT IO XML
hikitty = unHSPT $ unXMLGenT
        [hsx|
<html>
<head><title>CAT!!!</title></head>
<body><h1>"HELLO KITTY!!!"</h1>
</body>
</html> :: XMLGenT (HSPT XML (ServerPartT IO)) XML
|]
